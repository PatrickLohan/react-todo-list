import React from "react";
import TodoItem from "./TodoItem";

const App = () => (
  <div className="todo-list">
    <TodoItem />
    <TodoItem />
    <TodoItem />
    <TodoItem />
  </div>
);

export default App;
