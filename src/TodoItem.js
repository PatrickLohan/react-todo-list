import React from "react";

const TodoItem = () => (
  <div className="todo-item">
    <input type="checkbox" />
    <p>Placeholder text here</p>
  </div>
);

export default TodoItem;
